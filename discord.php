<?php

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/config.php';

use Discord\Discord;
use Discord\Parts\Channel\Message;
use React\EventLoop\Factory;
use React\Http\Browser;
use Discord\WebSockets\Event;
use Discord\Parts\WebSockets\MessageReaction;
use Discord\Parts\Channel\Channel;

$loop = Factory::create();
$browser = new Browser($loop);
$discord = new Discord([
    'token' => $configs["token_discord"],
    'loop' => $loop,
]);


$discord->on(Event::MESSAGE_REACTION_ADD, function (MessageReaction $reaction, Discord $discord) use ($configs) {
    $guild = $discord->guilds->get('id', '906873725138395146');

    $newchannel = $guild->channels->create([
        'name' => 'Ticket '.rand(0, 500),
        'type' => Channel::TYPE_TEXT,
        'parent_id' => $configs["id_channel_tickets"]
    ]);

    $guild->channels->save($newchannel)->done(function (Channel $channel) use ($reaction, $configs) {
        $user = $reaction->member->user;

        $channel->sendMessage("", false, $configs["first_message"])->done(function (Message $message) {
//            $message->react(':ping_pong:');
        });;
    });
});

$discord->run();